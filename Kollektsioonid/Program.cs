﻿
using System;
using System.Collections.Generic;
using System.Linq;

namespace Kollektsioonid
{

 

    class Program  // klassi algus
    {
        // protseduuri definitsioon algab siit
        static void Main(string[] args)
        {
            // veel mõned kollektsioonid
            if (false)
            {
                string[] vanakesed = { "Henn", "Ants", "Peeter" };

                // List

                List<string> nimed = new List<string>();

                nimed.AddRange(vanakesed);

                string nimi = "";
                do
                {
                    Console.Write("Anna mõni nimi: ");
                    if ((nimi = Console.ReadLine()) != "")
                    {
                        if (!nimed.Contains(nimi))
                        {
                            nimed.Add(nimi);
                        }
                    }

                }
                while (nimi != "");

                Console.WriteLine($"nimekirjas on {nimed.Count} nime: ");
                foreach (var n in nimed)
                {
                    Console.WriteLine($"\t{n}");
                }

                for (int i = 0; i < nimed.Count; i++)
                {
                    Console.WriteLine($"Number {i + 1} on {nimed[i]}");
                }

                //string[] nimedArray = nimed.ToArray();
                //List<string> teisedNimed = nimedArray.ToList();

                List<string> viisViimast = nimed.GetRange(nimed.Count - 5, 5);

                string viimased = string.Join(", ", viisViimast);
                Console.WriteLine(viimased);
            }
            // queue

            Console.WriteLine("Nii töötab järjekord");
            Queue<string> järjekord = new Queue<string>();
            // kes enne kohal see enne välja (FIFO)

            järjekord.Enqueue("Henn");
            järjekord.Enqueue("Ants");
            järjekord.Enqueue("Peeter");
            järjekord.Enqueue("Joosep");

            while( järjekord.Count > 0)
                Console.WriteLine(järjekord.Dequeue());

            Console.WriteLine("Nii töötab kuhi");
            Stack<string> kuhi = new Stack<string>();
            // KEs hiljem tuli, see ennem välja (LIFO)

            kuhi.Push("Henn");
            kuhi.Push("Ants");
            kuhi.Push("Peeter");
            kuhi.Push("Joosep");

            while (kuhi.Count > 0)
                Console.WriteLine(kuhi.Pop());

            // dictionary

            Dictionary<int, string> Nimestik 
                = new Dictionary<int, string>();

            Nimestik.Add(7, "Henn");
            Nimestik.Add(3, "Ants");
            Nimestik.Add(1, "Peeter");
            Nimestik.Add(4, "Jaak");
            Nimestik.Add(88, "Toomas");

            Console.WriteLine($"Nimestikus on {Nimestik.Count} tegelast");
            //Console.Write("Anna kood: ");
            //int kood = int.Parse(Console.ReadLine());

            //Console.WriteLine(Nimestik[kood]);

            foreach (var x in Nimestik.Keys.OrderBy(x => x))
            {
                Console.WriteLine($"võtmega {x} on {Nimestik[x]}");
            }

            foreach(var x in Nimestik.OrderBy(x => x.Value))
            {
                Console.WriteLine($"võti on {x.Key} ja nimi on {x.Value}");
            }

            // mõned nipid ja kavalused

            List<int> numbrid = new List<int>() { 13, 22, 3, 54, 25, 66, 18, 19, 23 };

            numbrid[3] += 5; // 54 asemele 59   // loe või muuda 4. elementi
            numbrid.Remove(13); // 13 minema - kustutab elemendi väärtusega
            numbrid.RemoveAt(0); // 22 minema - kustutab elemendi positsiooni järgi
            numbrid.RemoveAll(x => x % 2 == 0); // paarisarvud minema -  kustutab tingimuse alusel (hiljem selgitus)

            // need kaks on vaid neil kollektsioonidel, mis arvudest koosnevad
            int summa = numbrid.Sum();  // liidab kõik kokku, mis kollektsioonis
                                        // summa seda tüüpi, mis ka kollektsioon
            double average = numbrid.Average();
                                        // keskmine on alati double

            numbrid.Sort(); // pannakse järjekorda
                            // list peab koosnema võrreldavatest asjadest
            foreach (var x in numbrid.OrderBy(x => x))
                Console.WriteLine(x);
                // ei panda järjekorda, aga võetakse järjestuse alusel




        }
    }


} 